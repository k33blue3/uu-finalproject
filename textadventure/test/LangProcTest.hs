module LangProcTest where
import Test.HUnit
import LanguageProcessing

torchTest = [Token "take" ["grab", "pickup"], 
             Token "torch" []]
             
testSeveral = [[Token "walk" ["step"], Token "into" [], Token "the" [], Token "room" []],
                [Token "enter" [], Token "the" [], Token "room" []]]

tests :: Test
tests = TestLabel "Language Processing tests" $ TestList
    [
        testMatch, testMatchSeveral
    ]
    
testMatch :: Test
testMatch = TestLabel "MatchTest" $ TestList
    [
        TestLabel "Pick up torch" $ TestCase $ do
            let 
             test1 :: Bool
             test1 = match "aquire torch" torchTest
             
             test2 :: Bool
             test2 = match "take torch" torchTest
             
             test3 :: Bool
             test3 = match "" torchTest
             
            False @=? test1
            True @=? test2
            False @=? test3
    ]

testMatchSeveral :: Test
testMatchSeveral = TestLabel "MatchSeveralTest" $ TestList
    [
        TestLabel "Enter the room" $ TestCase $ do
            let 
             test1 :: Bool
             test1 = matchSeveral "enter the room" testSeveral
             
             test2 :: Bool
             test2 = matchSeveral "walk into the room" testSeveral
             
             test3 :: Bool
             test3 = matchSeveral "run away from the room" testSeveral
             
            True @=? test1
            True @=? test2
            False @=? test3
    ]
