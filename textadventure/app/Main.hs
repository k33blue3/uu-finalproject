import Engine

import LanguageProcessing

main :: IO ()
main = run

getTorch = [Token "get" ["grab", "take", "pickup"], Token "torch" []]
lookAround = [Token "look" ["search", "scout", "gaze", "examine"]]
killYou1 = [Token "light" ["ignite", "set"], Token "yourself" ["myself"], Token "on" [], Token "fire" []]
killYou2 = [Token "set" [], Token "yourself" ["myself"], Token "ablaze" []]
goFurther = [Token "go" ["walk", "continue"], Token "further" []]

killTroll1 = [Token "light" ["ignite", "set"], Token "troll" ["enemy"], Token "on" [], Token "fire" []]
killTroll2 = [Token "set" [], Token "troll" ["enemy"], Token "ablaze" []]

yorkCaveHandler :: Handler
yorkCaveHandler sInput gameState = execute
  (Node
    (match sInput lookAround)
    (Node
      (stateHasFlag "cave-pickedup-torch" gameState)
      (Action
        (putStrLn "There's nothing") (Just [])
      )
      (Action
        (putStrLn "Woah there's a torch") (Just []))
    )
    (Node 
      (match sInput getTorch)
      (Node
        (stateHasFlag "cave-pickedup-torch" gameState)
        (Action 
          (putStrLn "You already picked up torch stupid") (Just [])
        )
        (Action
          (putStrLn "You picked up the torch!")
          (Just [AddItem (Object "torch" True "A burning bright torch" Nothing Nothing (Just 10) Nothing), AddFlag "cave-pickedup-torch", RemoveFlag "starter-cave"])
        )
      )
      (Node
        (matchSeveral sInput [killYou1, killYou2])
        (Node
          (inventoryHasItem "torch" gameState)
          (Action
            (putStrLn "You light yourself on fire and promptly die.")
            (Just [SceneChange "end-scene"])
          )
          (Action
            (putStrLn "You don't have a torch.")
            (Just [])
          )
        )
        (Node
         (match sInput goFurther)
         (Action
          (putStrLn "You walk further inside the cave.") (Just [SceneChange "next-cave"])
        )
          (Action
            (putStrLn $ sInput ++ " does not make sense, try again.") (Just [])
          )
        )
      )
    )
  )

yorkCave2Handler :: Handler
yorkCave2Handler sInput gameState = execute 
  (Node (match sInput lookAround)
   (Node (inventoryHasItem "torch" gameState)
     (Action (putStrLn "The torch lights up the cave, you se a large troll infront of you!") (Just []))
     (Action (putStrLn "It is pitch black, you are lost") (Just [])))
   (Node (matchSeveral sInput [killTroll1, killTroll2])
     (Node (inventoryHasItem "torch" gameState)
       (Action (putStrLn "You set the troll on fire!") (Just [SceneChange "winning-scene"]))
       (Action (putStrLn "You don't have a torch, the troll kills you with one big stomp") (Just [SceneChange "end-scene"])))
     (Action (putStrLn "...") (Just []))))


yorkGameMap :: GameMap
yorkGameMap = createGameMap [
    Scene {name = "starter-cave",
    description = createDescriptions $ [("starter-cave", "You are in a pitch-black dank cave."), ("cave-pickedup-torch", "You can see more now as your torch lights up the cave, you see that it is possible to go further inside the cave.")],
    handler = yorkCaveHandler},
    Scene {name = "next-cave",
    description = createDescriptions $ [("next-cave", "You continue further inside the cave system...")],
    handler = yorkCave2Handler},
    Scene {name = "end-scene",
    description = createDescriptions $ [("end-scene", "You are dead, and you're quite happy about it. Press enter to continue")],
    handler = \_ _ -> return $ Nothing},
    Scene {name = "winning-scene",
    description = createDescriptions $ [("winning-scene", "You have won, thank the programmers, they are too lazy to make the demo any longer. Press enter to continue.")],
    handler = \_ _ -> return $ Nothing}
    ]

yorkInitialGameState :: GameState
yorkInitialGameState = initialGameState yorkGameMap ["end-scene"]

run = runGame "starter-cave" yorkInitialGameState
