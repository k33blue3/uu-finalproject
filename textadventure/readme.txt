When you're using stack, you need to set up your test suite manually.
This is a set-up I've come up with based on HUnit; I've made it as easy to
understand and use as possible.

First of all, in your "package.yaml", find the configuration for the project's tests.
By default, it's located at the very end of the file, and looks like this:

tests:
  PROJECTNAME-test:
    main:                Spec.hs
    source-dirs:         test
    ghc-options:
    - -threaded
    - -rtsopts
    - -with-rtsopts=-N
    dependencies:
    - PROJECTNAME

Where PROJECTNAME is your project name.
Modify the dependency list to include HUnit, like this:

tests:
  PROJECTNAME-test:
    main:                Spec.hs
    source-dirs:         test
    ghc-options:
    - -threaded
    - -rtsopts
    - -with-rtsopts=-N
    dependencies:
    - PROJECTNAME
    - HUnit >= 1.6 && < 1.7

Once you've done that, go ahead and move the .hs files I've
provided in the test folder to your stack project's test folder,
overwriting the old Spec.hs.

"ExampleTest.hs" is an example of a test module; you may use it as a basis for
your own test modules.
Each test module must be located in the "test" directory.
You should divide test modules according to the primary components of your program:
for example, you could have "EngineLogicTest.hs", "ParserTest.hs" and "AITest.hs".

Each test module should define 'tests :: Test' which represents all tests
associated with the module, which are together labeled with the module name
using 'TextLabel'.
Each test of a test module can have further subtrees of labeled tests, just like
in ExampleTest.hs.

"Spec.hs" is the main file for testing. It's what stack will run upon "stack test".
In Spec.hs, you should import each test module you've created qualified,
and add each module's 'tests' to the 'allTests' list. 'main' of Spec.hs will run
all tests within the 'allTests' list, and report any failures or errors.
As provided, Spec.hs will only run 'tests' of ExampleTest. Once you add test modules of your own, you should remove 'ExampleTest.tests' from the 'allTests' list.

You can try the set-up out by running "stack test". HUnit should report the number
of tests run, and any failures or errors. If any test fails or errors, stack will tell you that the test suite failed; otherwise, it will tell you that the test suite passed.