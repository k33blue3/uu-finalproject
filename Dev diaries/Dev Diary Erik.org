* Eriks Diary
** 19/02/2020 ~ 1hr
   We had the meeting with Love. He gave us some files aswell as recomended some tools to be used.
** 20/02/2020 ~ 1hr
   Had meeting on Discord
** 21/02/2020 ~ 5-6hr
   Studied the code for handler function, and added a prompt for when the input was not valid.
   
   TLDR; version of what i have done.
*** Programming changes
    TLDR: *New functions, data-structures  and constants*
    
    /Room data structure/ struct
    # Data structure to contain contents of a room / be a room

    /listofrooms/  const 
    # Some pre-made rooms

    /yorkGameMap/ func 
    # Added (east, west, north, south)

   
    More detailed description.
*** Text explaining my decisions
    Studied the code for handler function, and added a prompt for when the 
    input was not valid. 
    
    I also started to add more  *(room /// cave / areas/)* , 
    i added east, and west. I plan to add more directions and rooms.
    
    I added a Data structure that will contain everything a player can find in rooms.
    The data structure is named Room it has a constructor with the name "Space"
    that holds a list of Rooms such that the content of a room can be contained 
    in it.
    
    The game map has now a map for every direction /(east, west, north, south)/
    
    i am planning on making a procedural generator for rooms and the content 
    of then. But now in the beginning i am just making some 
    testing rooms such that i learn the general gist of how the game should work,

    I am also going to change the handler to be more versatile and simple to 
    create new situations. 

    In general i want everything to be able to be generated. 
    
** 22/02/2020 ~ 1hr
   Tinkered on my old way of creating Objects in Engine.hs

** 23/02/2020 ~ 3hr
Updated and modified the data type of objects such that one can more easily create objects, characters and such, and distinguish between them.

Example of how to create a sword.
#+BEGIN_SRC haskell
Object "Sword of light" Thing [(Sword "A white sword, made of elf steel" 100)]
#+END_SRC

Also tried to describe the item system to Marcus so he could use it.
** 25/02/2020 ~15 min
Quick meeting over discord

** 26/02/2020 ~ 4hr
Had a meeting with Love, where he told us that we were on the right path, what we should work on and prioritize and such. 

Worked a couple of hours when i got home on a alternative story.
** 28/02/2020 ~ 8hr 
   Continued to work on the story and learn the workflow of our engine
** 01/03/2020 ~ 2hr
Was debugging my story / handlers all night, could not figure out what was wrong.

** 02/03/2020 ~4hr
Lost my story when i deleted and cloned the Repo without saving, however fixed the bug when i started working on it from scratch. Story will now be a continuation on the original cave story. 

** 03/03/2020 ~ 5hr
Wrote on the documentation the whole day, started around 10:00 slowly and ended around 16:00. 
Updated and wrote sections in more detail as well as added new parts.


