module Engine where

import Data.Map.Strict (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

{- GameState stores information about the state of the game.
   It stores the game map, a list of scenes that end the game, the player inventory and a list of flags.
-}
data GameState = GameState {
    gameMap   :: GameMap,
    endScenes :: Set String,
    inventory :: Map String Object,
    flags     :: Set String}

instance (Eq GameState) where
    x == y = (gameMap x == gameMap y) && (endScenes x == endScenes y) && (inventory x == inventory y) && (flags x == flags y)
    
instance (Show GameState) where
    show a = "A GameState"

type Handler = String -> GameState -> IO (Maybe [Impact])
type GameMap = Map String Scene
type CondDesc = Map String String

{- Scene represents a scene in the game.
   It has a name, a description and a function that handles user input in the scene.
-}
data Scene = Scene {
    name        :: String
  , description :: CondDesc
  , handler     :: Handler
  }
  
instance (Eq Scene) where
    x == y = (name x) == (name y)

instance (Show Scene) where
    show s = show (name s)

{- Impact defines an effect the game has on a state.
   Impact has five different constructors for each effect that a game can have on a state.
-}
data Impact
  = SceneChange String
  | AddItem Object
  | AddFlag String
  | RemoveFlag String
  | RemoveItem String


{- Object defines an object.
   The object type stores a number of attributes for the object.
   Example:
    Object "Cup", False, "A red cup with a handle", ("drink", drinkFromCup), 0, 10, Nothing
-}
data Object = Object {
  itemName :: String,
  isAlive :: Bool,
  objDescription :: String,
  -- What it can interact with first, and second a function 
  -- that preforms that interaction.
  interactWith :: Maybe [(String, (Object -> Object))],
    objDamage :: Maybe Int,
    objHealth :: Maybe Int,
    objInventory :: Maybe [Object]
  }
  | Empty
  
instance (Show Object) where
    show a = show $ (itemName a)
  
instance (Eq Object) where
    x == y = (itemName x == itemName y) && (isAlive x == isAlive y) && (objDescription x == objDescription y)

{- FlowTree defines a binary tree that stores IO Actions and Impacts
   It utilizes a tree structure to store the IO Actions and Impacts that should be played. Each branch in the tree is a path that the dialog can follow.
   INVARIANT: Every path in the tree ends with an Action.
-}
data FlowTree = Node {dir :: Bool, left :: FlowTree, right :: FlowTree}
              | NodeAction {text :: (IO ()), dir :: Bool, left :: FlowTree, right :: FlowTree}
              | Action {text :: (IO ()), impacts :: (Maybe [Impact])}
              


{- createGameMap scenes
    Creates a game map that the game can be run on
    RETURNS: a map built from scenes where each element in scenes is a Scene
    EXAMPLES:
    createGameMap [Scene {name = "starter-cave", description = createDescriptions $ [("starter-cave", "You are in a pitch-black dank cave."), ("cave-pickedup-torch", "You can see more now as your torch lights up the cave")], handler = yorkCaveHandler}] =
    [ "starter-cave", Scene {name = "starter-cave", description = createDescriptions $ [("starter-cave", "You are in a pitch-black dank cave."), ("cave-pickedup-torch", "You can see more now as your torch lights up the cave")], handler = yorkCaveHandler} ]
-}
createGameMap :: [Scene] -> GameMap
createGameMap scenes = Map.fromList (treatScenes scenes)
-- turns a list of type Scene into a list of tuples with the name of the scene and the scene itself
  where --VARIANT: length of scenes
    treatScenes [] = []
    treatScenes (scene:scenes) = ((name scene), scene) : treatScenes scenes

{- createDescriptions l
   Takes a list of tuples with strings and returns a map based on them
   RETURNS: A map where the first element in each tuple of l maps to the second element
   EXAMPLES: 
            createDescriptions [("Hello", "There")] = fromList [("Hello","There")]
            createDescriptions [] = fromList []
            createDescriptions [("Hello","There"), ("Hello", "There, General Kenobi")] = fromList [("Hello", "There")]
-}
createDescriptions :: [(String, String)] -> CondDesc
createDescriptions l = Map.fromList l

{- runGame scene state
   Start a Scene with a GameState
   RETURNS: ()
   SIDE-EFFECTS: reads from and prints to the console.
-}
runGame :: String -> GameState -> IO ()
runGame sceneStr state = do
    let map = gameMap state
    case Map.lookup sceneStr map of
        Nothing -> putStrLn $ "Failed to find scene" ++ sceneStr
        Just scene -> do
            printDescription (description scene) (Set.toList $ flags state)
            sInput <- getLine
            result <- (handler scene) sInput state
            case result of
                Nothing -> putStrLn "Done"
                (Just impacts) -> let (newState, newScene) = stateIter impacts (state, sceneStr) in runGame newScene newState
            
    where
        stateIter :: [Impact] -> (GameState, String) -> (GameState, String)
        stateIter [] a = a
        stateIter (x:xs) a = stateIter xs $ updateState x a
        
        printDescription :: CondDesc -> [String] -> IO ()
        printDescription descriptions [] = return ()
        printDescription descriptions (x:xs) = case Map.lookup x descriptions of
                                                                                Nothing -> printDescription descriptions xs
                                                                                Just text -> do
                                                                                                putStrLn text
                                                                                                printDescription descriptions xs
                                                    


{- updateState imp (state, str)
   Takes a GameState and modifies it based on the given Impact
   RETURNS: A tuple consiting of the new GameState created when imp is applied to state and the next scene to be played. 
            The scene will be str if the imp was not a SceneChange. Othwerwise it will be what the SceneChange changed it to. 
   EXAMPLES: updateStatate (SceneChange "newScene") (state, "oldScene") == (state, "newScene")       
-}
updateState :: Impact -> (GameState, String) -> (GameState, String)
updateState imp (state, str) = case imp of
    SceneChange str -> (state, str)
    AddItem item -> let 
        newInv = Map.insert (itemName item) item (inventory state)
        newState = GameState (gameMap state) (endScenes state) newInv (flags state)
        in
            (newState, str)
    AddFlag sFlag -> let
        newFlags = Set.insert sFlag (flags state)
        newState = GameState (gameMap state) (endScenes state) (inventory state) newFlags
        in
            (newState, str)
    RemoveItem sItem -> let
        newInv = Map.delete sItem (inventory state)
        newState = GameState (gameMap state) (endScenes state) newInv (flags state)
        in
            (newState, str)
    RemoveFlag sFlag -> let
        newFlags = Set.delete sFlag (flags state)
        newState = GameState (gameMap state) (endScenes state) (inventory state) newFlags
        in
            (newState, str)

{- initalGameState gamemap endscenes
    creates a GameState based on a GameMap and a list of endscenes
    RETURNS: a GameState based on gameMap and endScenes
    EXAMPLES:
    initialGameState gamemap endscenes = GameState {gamemap, (Set.fromList endscenes), Map.empty, (Set.fromList (getKeys gamemap))}
-}
initialGameState :: GameMap -> [String] -> GameState
initialGameState gamemap endscenes = GameState gamemap (Set.fromList endscenes) Map.empty (Set.fromList $ getKeys gamemap)
  where 
    getKeys :: Map String Scene -> [String]
    getKeys m = map fst $ Map.toList m -- turns a map of keys and values into a list of keys in the given map

{- stateHasFlag flag state
   Determines whether a GameState has a flag 
   RETURNS: TRUE if state has flag, FALSE if it does not
   EXAMPLES:
   stateHasFlag "Kitchen" (GameState {x, y, z, (fromList ["Kitchen", "Living Room", "Bedroom"])}) = True
   stateHasFlag "Bathroom" (GameState {x, y, z, (fromList ["Kitchen", "Living Room", "Bedroom"])}) = False
-}

stateHasFlag :: String -> GameState -> Bool
stateHasFlag sFlag state = Set.member sFlag (flags state)

{- inventoryHasItem item state
   Determines whether a GameState contains an item
   RETURNS: TRUE if state has item, FALSE if it does not
   EXAMPLES:
   inventoryHasItem "Chair" (GameState {x, y, (fromList [("Chair", Chair), ("Cup", "Cup")]), z}) = True
   inventoryHasItem "Table" (GameState {x, y, (fromList [("Chair", Chair), ("Cup", "Cup")]), z}) = False
-}
inventoryHasItem :: String -> GameState -> Bool
inventoryHasItem sItemName state = Map.member sItemName (inventory state)

{- execute tree
   Recursively execute a path in a FlowTree.
   RETURNS: The list of Impacts specfied in the action that gets run when following the path of bools in tree.
   SIDE-EFFECTS: performs IO actions specified in the tree. 
   EXAMPLES: execute Action (putStrLn "Hello" [SceneChange "nextScene"] == IO ([SceneChange "nextScene"]) and prints "Hello" to the console.
  -}
execute :: FlowTree -> IO (Maybe [Impact])
--VARIANT: Height of tree
execute (Node b left right) = if b then execute left else execute right
execute (NodeAction text b left right) = do
                                            text
                                            if b then execute left else execute right
execute (Action text impacts) = do 
                                  text
                                  return impacts
