module LanguageProcessing where

import Data.Char

--Used to represent a word and all other words that mean the same thing.
data Token = Token {word :: String, synonyms :: [String]}
    deriving (Show)

{- isSynonym str token
    Check if a string is a synonym to a token
    RETURNS: True if str is a synonym of token, False otherwise
    EXAMPLES: isSynonym "hej" (Token "greetings" ["hej"]) == True
              isSynonym "nej" (Token "greetings" ["hej"]) == False
-}
isSynonym :: String -> Token -> Bool
isSynonym sInput token = (sInput `elem` (synonyms token)) || sInput == (word token)

{- match input sentence
   match user input to a sentence
   RETURNS: True if input contains the same words or synonyms to the words in each token of sentence, False otherwise
   EXAMPLES: match "hej" [Token "hej" ["hallå"]] == True
             match "hallå" [Token "hej" ["hallå"]] == True
             match "" [] == True
             match "hej då" [Token "hej" ["hallå"], Token "då" ["igår"]] == True
             match "hej imorgon" [Token "hej" ["hallå"], Token "då" ["igår"]] == False
-}
match :: String -> [Token] -> Bool
match sInput tokens = matchAux (words sInput) tokens
    where
        matchAux :: [String] -> [Token] -> Bool
        matchAux a b | (length a) /= (length b) = False
        matchAux [] [] = True
        matchAux (x:xs) (y:ys) = isSynonym x y && matchAux xs ys

{- matchSeveral input sentences
   match user input to several sentences
   RETURNS: True if input matches any of the sentences, False otherwise
   EXAMPLES: matchSeveral "god morgon" [[Token "god" ["good"], Token "dag" ["day"]],[Token "god" ["good"], Token "morgon" ["morning"]]] == True
             matchSeveral "" [] == True
-}
matchSeveral :: String -> [[Token]] -> Bool
matchSeveral "" [] = True;
matchSeveral sInput [] = False
matchSeveral sInput (x:xs) = match sInput x || matchSeveral sInput xs
