-- This is an example of a test module, located in the "test" directory.
module ExampleTest where

import Test.HUnit

-- In test modules, you're able to import any module from your dependencies,
-- as well as any (sub)module in the "src" directory.
-- You are NOT able to import the Main.hs file in the "app" directory.
import Text.Read (readMaybe)
import Data.List (sortOn)


-- The tests associated with the module, labeled with the module name.
tests :: Test
tests = TestLabel "ExampleTest" $ TestList
  [ testReadMaybe
  , testSortOn
  ]


-- Tests for "readMaybe"
testReadMaybe :: Test
testReadMaybe = TestLabel "readMaybe" $ TestList
   [ TestLabel "should parse proper strings properly" $ TestCase $ do
      let
        test1 :: Maybe [Double]
        test1 = readMaybe "[1,2.5,3]"

      test1 @=? Just [1,2.5,3]
  , TestLabel "should return Nothing on improper strings" $ TestCase $ do
      let
        test2 :: Maybe [Double]
        test2 = readMaybe "[1,2.5,3]"

        test3 :: Maybe [Double]
        test3 = readMaybe "[1,True,3]"

      Nothing @=? test2
      Nothing @=? test3
  ]

-- Tests for "sortOn"
testSortOn :: Test
testSortOn = TestLabel "sortOn" $ TestList
  [ TestLabel "should sort properly" $ TestCase $ do
      let
        test1 :: [Int]
        test1 = sortOn id [5,1,4,2,3]

      [1,2,3,4,5] @=? test1
  , TestLabel "should be a stable sort" $ TestCase $ do
      let
        test2 :: [(Int, String)]
        test2 =
          sortOn
            fst
            [(3, "is"), (1, "This"), (5, "sort"), (3, "a"), (4, "stable")]

      [(1, "This"), (3, "is"), (3, "a"), (4, "stable"), (5, "sort")] @=? test2
  ]
