import Test.HUnit
import System.Exit

-- You'll need to import every test module, using qualified to avoid conflicts.
import qualified LangProcTest
import qualified EngineTests
-- import qualified Example3Test
-- ...

-- The tests to be run by "stack test".
-- This list should contain 'tests' from each imported test module.
allTests :: [Test]
allTests =
  [LangProcTest.tests,
   EngineTests.tests]

main :: IO ()
main = do
  result <- runTestTT $ TestList allTests
  -- When we have any failures or errors...
  if (failures result + errors result > 0) then
    -- Exit the program using 'exitFailure'. By doing so,
    -- we signal stack that testing has failed.
    exitFailure
  else
    return ()
