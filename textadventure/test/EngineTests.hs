module EngineTests where
import Test.HUnit
import Engine

import Data.Map.Strict (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

baseState = GameState (Map.empty) (Set.empty) (Map.empty) (Set.empty)
nonEmptyState = GameState (Map.empty) (Set.fromList ["Test"]) (Map.empty) (Set.empty)

exists a = case a of
                Nothing -> False
                (Just b) -> True

tests :: Test
tests = TestLabel "Engine tests" $ TestList
    [
        testUpdateState, testCreateGameMap, testCreateDescriptions, testCreateState
    ]

testCreateState :: Test
testCreateState = TestLabel "InitalState" $ TestList
    [
        (TestLabel "BaseState" $ TestCase $ do
            let
             test :: GameState
             test = initialGameState Map.empty []
             
            baseState @=? test),
            
        (TestLabel "Non empty state" $ TestCase $ do
            let
             test :: GameState
             test = initialGameState Map.empty ["Test"]
             
            nonEmptyState @=? test)
    ]

testCreateDescriptions :: Test
testCreateDescriptions = TestLabel "CreateDesciptions" $ TestList
    [
        (TestLabel "Empty Description" $ TestCase $ do
            let
             test :: CondDesc
             test = createDescriptions []
             
            Map.empty @=? test),
            
        (TestLabel "Non empty description" $ TestCase $ do
            let
             test :: CondDesc
             test = createDescriptions [("Test", "TestDesc")]
             
            True @=? exists (Map.lookup "Test" test))
    ]

testCreateGameMap :: Test
testCreateGameMap = TestLabel "Create GameMap" $ TestList 
    [
        (TestLabel "Empty Map" $ TestCase $ do
            let
             test :: GameMap
             test = createGameMap []
         
            Map.empty @=? test),
        
        (TestLabel "Non empty Map" $ TestCase $ do
            let
             test :: GameMap
             test = createGameMap [Scene "Test" (Map.empty) (\_ _ -> return Nothing)]
         
            True @=? (exists $ Map.lookup "Test" test))]
    
testUpdateState :: Test
testUpdateState = TestLabel "Update state" $ TestList
    [
        (TestLabel "SceneChange" $ TestCase $ do
            let 
             test :: String
             test = snd $ updateState (SceneChange "new") (baseState, "old")
                
            "new" @=? test),
            
        (TestLabel "AddItem" $ TestCase $ do
            let
             test :: GameState
             test = fst $ updateState (AddItem (Object "Test" False "Description" Nothing Nothing Nothing Nothing)) (baseState, "")
             
             item = Map.lookup "Test" (inventory test)
             
            (Just (Object "Test" False "Description" Nothing Nothing Nothing Nothing) @=? item)),
            
        (TestLabel "AddFlag" $ TestCase $ do
            let
             test :: GameState
             test = fst $ updateState (AddFlag "TestFlag") (baseState, "")
             
             item = Set.member "TestFlag" (flags test)
             
            True @=? item),
        
        (TestLabel "RemoveItem" $ TestCase $ do
            let
             state :: GameState
             state = fst $ updateState (AddItem (Object "Test" False "Description" Nothing Nothing Nothing Nothing)) (baseState, "")
             newState = fst $ updateState (RemoveItem "Test") (state, "")
             
             contains = inventoryHasItem "Test" newState
             
            False @=? contains),
            
        (TestLabel "RemoveFlag" $ TestCase $ do
            let
             state :: GameState
             state = fst $ updateState (AddFlag "TestFlag") (baseState, "")
             newState = fst $ updateState (RemoveFlag "TestFlag") (state, "")
             
             contains = stateHasFlag "TestFlag" newState
             
            False @=? contains)
    ]